# CCA
CCA is a MATLAB code for canonical correlation analysis algorithm - both non-private and differentially-private. Currently it contains a function that computes CCA, PCA, DPCCA and DPPCA subspaces and performance indices. The details of the performance indices are described inside the function.

## Contact

* Hafiz Imtiaz (hafiz.imtiaz@rutgers.edu)

## Dependencies

The codes are tested on MATLAB R2016a. The helper functions are included in the function files.


## Downloading

You can download the repository at https://gitlab.com/hafizimtiaz/dp-cca.git, or using

```
$ git clone https://gitlab.com/hafizimtiaz/dp-cca.git
```

## Installation

Copy the function in your working directiory or add the directory containing the functions in MATLAB path.


## License

MIT license

## Acknowledgements

This development of this collection was supported by support from the
following sources:

* National Institutes of Health under award 1R01DA040487-01A1

Any opinions, findings and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of National Institutes of Health.
